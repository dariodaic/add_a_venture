class Player
  attr_accessor :name, :items, :information,:lives, :location

  def initialize(name, items, information, lives)
    @name = name
    @items = items
    @information = information
    @lives = lives
  end

  def add_item_to_player_items(item)
    @items << item
  end

  def find_player_item(item_query)
    @items.detect { |item| item == item_query }
  end

  def add_player_information(information)
    @information << information
  end

  def find_player_information(information)
    @information.detect { |info| info == information}
  end
end

class Room
  attr_accessor :reference, :name, :description, :connections

  def initialize(reference, name, description, connections)
    @reference = reference
    @name = name
    @description = description
    @connections = connections
  end
end

class Items
  attr_accessor :name

  def initialize(name)
    @name = name
  end
end

class Door
  attr_accessor :name, :combo, :state

  def initialize(name, combo, state)
    @name = name
    @combo = combo
    @state = state
  end
end

class Creatures
  attr_accessor :name, :items, :information

  def initialize(name, items, information)
    @name = name
    @items = items
    @information = information
  end

  class Panda < Creatures
    def conversation
      puts "(You step bravely towards him.)"
      puts "(It looks like he is in some sort\nof meditation.)"

      while true
        puts "(What should you ask him?)"
        puts "|1.| Who are you?"
        puts "|2.| What do you know about that golden chest?"
        puts "|3.| Farewell, Great #{@name}!"
        print ">"
        ask = gets.chomp

        if ask == "1"
          puts "I am kung-fu panda. Also known as great warrior #{@name}."
          puts "I beat up a Rhino once. Not impressed? I beat up a giraffe to!"
          puts "You look like a nice lad......don't fuck with me."
          puts "What do you want?"
        elsif ask == "2"
          puts "I tried to break into it. I am sure something of great value is stored inside."
          puts "But you know I am a fighter, and a really strong one."
          puts "Not some disgusting thief. If you know what I mean that is.... "
          puts "*frowns*"
        elsif ask == "3"
          puts "Buzz-off kid.. I'm leveling-up here.."
          puts "Bring me trstika next time!"
          break
        else
          puts "Smartass..."
        end
      end
    end
  end

  class FoxyLady < Creatures

    def exit_method(current_room)
      current_room
    end

    def conversation(player)
      while true
        hAiRpIn = player.items.detect {|item| item == "Hairpin"}
        puts "You see a fine looking, well dressed fox lady."
        puts "Silk dress, shiny shoes, rings and things, hair stuffed with hairpins. It looks like a state of art."
        puts "She is finde indeed. You use your charm and find out that her name is Ice La Fox."
        puts "What a beautiful name, you think to yourself.\n\n"

        if !hAiRpIn
          puts "|1.| Dear maam Ice La Fox, i humbly admit I have not witnessed such a beauty to this day!"
          puts "Do you mind giving me a hairpin of yours so I shall reminisce this beauty for the rest of my life!?"
          puts "|2.| Do you know anything about this place dear maam?"
          puts "|3.| (You decide to pursue your adventure. Enough with this crazy talk.)"
        else
          puts "|1.| Do you know anything about this place dear maam?"
          puts "|2.| (You decide to pursue your adventure. Enough with this crazy talk.)"
        end
        print ">"
        choice = gets.chomp

        if choice == "1" && !hAiRpIn
          puts "Aren't you a sweet one?!"
          puts "There you go little boy. (She winks at you.)"
          puts "She gives you her hairpin."
          puts "Well done you stud! You give yourself a pat on the back."
          @items = @items - ["Hairpin"]
          player.add_item_to_player_items("Hairpin")
        elsif (choice == "2" && !hAiRpIn) || (choice == "1" && hAiRpIn)
          puts "This used to be an old mining site for a big factory whice is owned by a man who is my husband."
          puts "It is just a part of his international conglomerate. He is one of the wealthiest man in the world."
          puts "It is most natural that he owns the most beautiful women in the world, too. (she chuckels..)"
          puts "I am waiting for him. He just left to give a lecture to a site manager about some business stuff."
          puts "Did I mention he is very rich!?!"

          while true
            puts "|1.| Did you see anything around here that might be useful to a poor soul like me, maam la Fox?"
            puts "|2.| Would it happen that you know something about that locked chest in the Old room little further ahead, maam la Fox?"
            puts "(|3.| Go to the beginning of the conversation.)"
            print ">"

            question = gets.chomp

            if question == "1"
              puts "I noticed that box of old and dirty stuff there in the corner."
              puts "It seems to have contained things like an old tool, a handy bag, dusty books and such..."
              puts "I don't know who would want to dig inside it though."
              puts "It looks sooo...uggghhhh."
              if !player.information.detect { |info| info == "Bag"}
                @information = @information - ["Bag"]
                player.add_player_information("Bag")
              end
            elsif question == "2"
              puts "What chest!?"
              puts "I am no poor sould in need of some dirty and ragged stuff."
              puts "Phew..(She looks the other way..)"
            elsif question == "3"
              puts "(You decide to go to the beginning of the conversation."
              break
            else
              puts "Smartass....."
            end
          end
        elsif (choice == "3" && !hAiRpIn) || (choice == "2" && hAiRpIn)
          puts "(You can not listen to the word she says anymore...but she could be a fiiiine company.):O ^^"
          break
        else
          puts "Smartass....."
        end
      end
    end
  end
end

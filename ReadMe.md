  Practicing Ruby #2

  ADD A VENTURE  -  text-based adventure

  AAV is my first Ruby project I took on my own.
  I wrote it two to three months after I
  started learning programming (Ruby is my first 
  programming language).
  
  There is a lot that can be refactored in the code
  but I want to be able to look at it after a year or
  two.
  
  As far as the game itself is concerned it is a 
  mystery/puzzle game (I'm a big fan of Sherlock Holmes).
  I would say puzzles are easy to solve if one is
  focused. In the end the 'game' displays amount of time
  it takes a player to solve it.

  Written: ~ 1.3.2013.

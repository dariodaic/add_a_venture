class Dungeon
  require_relative 'add_a_venture_classes'

  attr_accessor :player

  def initialize(name, items, information, lives, gate_name, combo, state)
    @player = Player.new(name, items, information, lives)
    @rooms = []
    @creatures = []
    @items = []
    @gates = Door.new(gate_name, combo, state)
    @game_time = Time.now
  end

  # Methods for manipulating a ROOM
  def add_a_room(reference, name, description, connections)
    @rooms << Room.new(reference, name, description, connections)
  end

  def find_a_room(reference)
    @rooms.detect { |room| room.reference == reference }
  end

  # Methods for manipulating a CREATURE
  def add_creatures
    @creatures << Creatures::Panda.new("Pandus", [], [])
    @creatures << Creatures::FoxyLady.new("Ice La Fox", ["Hairpin"], ["Bag"])
  end

  def find_a_creature(creature_name)
    @creatures.detect { |creature| creature.name == creature_name }
  end

  # Methods for manipulating an ITEM
  def add_items
    @items << Items.new("Uon stone").name
    @items << Items.new("Wot stone").name
    @items << Items.new("Reid stone").name
    @items << Items.new("Bag").name
  end

  def find_an_item(wanted_item)
    @items.detect {|item| item == wanted_item}
  end

  # M E T H O D S   F O R   A   G A M E
  def start_room(reference)
    @player.location=(reference)
    action_in_current_room
  end

  def go(direction)
    puts "You go to #{direction.to_s}!\n\n"
    next_player_location = find_a_room(@player.location).connections[direction]
    @player.location = next_player_location
    action_in_current_room
  end

  def action_in_current_room
    show_current_room_description
    behaviour_in_room
    if @player.lives > 0 && @gates.state == "closed"
      go_next_room
    elsif @player.lives > 0 && @gates.state == "opened"
      happy_end(@player.name)
    else
      sad_end(@gates.name, @player.name)
    end
  end

  def show_current_room_description
    current_room = find_a_room(@player.location)
    puts current_room.name
    puts "#{@player.name} you are in a #{current_room.name}."
    puts current_room.description
  end

  def behaviour_in_room
    current_room = find_a_room(@player.location)
    if current_room.reference == :lobby_room
      lobby_room
    elsif current_room.reference == :storage_room
      storage_room
    elsif current_room.reference == :machinery_room
      machinery_room
    else
      old_room
    end
  end

  def go_next_room
    puts "Where would you like to go next?"
    puts "[storage/old/lobby or machinery room]"
    next_room = ""
    while true
      print ">> "
      next_room = gets.chomp.to_s.downcase
      if (next_room + "_room").to_sym == @player.location
        puts "You are already in this location."
        puts "Enter a new direction...asshole."
      elsif next_room == "items"
        puts "(Your items: #{@player.items}.)"
        puts "Enter the name of the next room:"
      elsif next_room == "storage" || next_room == "machinery" || next_room == "old" || next_room == "lobby"
        go(next_room.to_sym)
      else
        puts "Insert name of the room in the right way."
        puts "-> roomname <-"
      end
    end
  end

  def lobby_room
    puts "It must have been an information room of some kind back when old Greek gods walked the earth."
    puts "You guess it must have been a recepcionist room back when miners used to visit this place."
    puts "There is a foxy lady in the room with you. She sure looks like a dame."

    while true
      bag_info = @player.find_player_information("Bag")
      if bag_info
        puts "|1.| Talk to fox lady."
        puts "|2.| Inspect the box!"
        puts "|3.| Just leave the room."
        print "> "
      else
        puts "|1.| Talk to the lady."
        puts "|2.| Leave the room."
        print "> "
      end

      choice = gets.chomp

      if choice == "1"
        ice_la_fox = @creatures.detect { |creature| creature.name == "Ice La Fox"}
        ice_la_fox.conversation(@player)
      elsif choice == "2" && @player.find_player_information("Bag")
        if !@player.items.detect { |item| item == "Bag"}
          puts "You search through the stuff inside."
          puts "Lots of old keywrenches, used worker clothes, and an old but sturdy bag."
          puts "Who knows when it might come in handy. You take the bag."
          @player.add_item_to_player_items("Bag")
        else
          puts "You picked up the bag. There is nothing of interest for you in the box."
        end
      elsif choice == "3" || (choice == "2" && !bag_info)
        break
      else
        puts "Smartass....."
      end
    end
  end

  def storage_room
    puts "There is a creature in the room with you."
    while true
      uon_stone = find_an_item("Uon stone")
      if uon_stone
        puts "There is an odd Uon stone on the old worn out table on the other side."
        puts "|1.| Talk to the creature."
        puts "|2.| Take a stone."
        puts "|3.| Leave the room."
        print "> "
      else
        puts "|1.| Talk to the creature."
        puts "|2.| Leave the room."
        print "> "
      end

      move = gets.chomp

      if move == "1"
        panda = find_a_creature("Pandus")
        panda.conversation
      elsif move == "2" && !@player.items.detect { |uonstone| uonstone == "Uon stone" }
        if @player.find_player_item("Bag") == "Bag"
          @player.add_item_to_player_items("Uon stone")
          @items = @items - ["Uon stone"]
          puts "You took the Uon stone and put it in your handy Kozmo Bag."
        else
          puts "You don't have anything to put the stone into."
          puts "It is too heavy to carry around."
        end
      elsif (move == "3" && !@player.items.detect { |uonstone| uonstone == "Uon stone" }) || (move == "2" && @player.items.detect { |uonstone| uonstone == "Uon stone" })
        break
      else
        puts "Smartass..."
      end
    end
  end

  def machinery_room
    puts "Room is deserted and filled with stale smell of oil."
    while true
      wot_stone = find_an_item("Wot stone")
      if wot_stone
        puts "There is a Wot stone inside of someting that resembles an oven."
        puts "|1.| Take the stone."
        puts "|2.| Leave the room."
        print "> "
      else
        puts "Oven is empty, you took the Wot stone."
        break
      end

      move = gets.chomp

      if move == "1"
        if @player.find_player_item("Bag") == "Bag"
          @player.add_item_to_player_items("Wot stone")
          @items = @items - ["Wot stone"]
          puts "It seemed the oven was creamed, but you managed to open it with a strong hit on its top left corner."
          puts "You take a Wot stone and stash it safely in your Kozmo bag."
        else
          puts "You don't have anything to put the stone into."
          puts "It is too heavy to carry around."
        end
      elsif move == "2"
        break
      else
        puts "Smartass...."
      end
    end
  end

  def old_room
    hAiRpIn = @player.find_player_item("Hairpin")
    reid_stone = @player.find_player_item("Reid stone")
    puts "There is a chest in the room with you."
    puts "There is a keylock in the door."
    while @player.lives > 0 && @gates.state == "closed"
      puts "What would you like to do?"
      puts "|1.| Inspect a chest!"
      puts "|2.| Go figure out the gates.."
      puts "|3.| Leave the room."
      print "> "

      choice = gets.chomp

      if choice == "1" && !reid_stone
        while true
          if !hAiRpIn
            puts "It is old, dusty chest made from hard wood."
            puts "Thing is too heavy to lift and too tough to break."
            puts "There is however an ordinary keylock on it."
            puts "|1.| Try to open it."
            puts "|2.| Go away..."
            print "> "
          else
            puts "(You are armed with a hairpin...)"
            puts "(You smirk...you gonna own it!)"
            puts "|1.| Try to hack the chest!"
            puts "|2.| Go away..."
            print "> "
          end

          choice = gets.chomp

          if choice == "1" && !hAiRpIn
            puts "The chest won't budge."
            puts "Hmmm..."
          elsif choice == "1" && hAiRpIn
            puts "(HA!! You cracked it!!)"
            puts "I am Da BOSS!!"
            puts "(You yell, but there is no one to witness your magnificent act.)"

            while true
              reid_stone = @player.find_player_item("Reid stone")
              bag = @player.find_player_item("Bag")
              if !reid_stone
                puts "Will you take the Reid stone?"
                puts "|1.| Take the Reid stone."
                puts "|2.| (Go back.)"
                print ">"
              else
                puts "You have Reid stone. What else do you want in your life?!?"
                puts "|1.| (Go back.)"
                print ">"
              end

              choice = gets.chomp

              if choice == "1" && !bag
                puts "You have nothing to put the stone into and it is far too heavy to carry it around."
              elsif (choice == "1" && bag) && !reid_stone
                puts "This Kozmo bag really comes in handy."
                puts "(You pick up the Reid stone.)"
                @player.add_item_to_player_items("Reid stone")
                @items = @items - ["Reid stone"]
              elsif choice == "1" && reid_stone
                break
              elsif choice == "2" && !reid_stone
                break
              else
                puts "Smartass..."
              end
            end
          elsif choice == "2"
            break
          else
            puts "Smartass....."
          end
        end
      elsif choice == "1" && reid_stone
        puts "Your enemy is sentenced to a losers existence, while the mask of triumph gives your face a hero-like beauty!"
        puts "You decide to leave it be.."
      elsif choice == "2"
        puts "There is a big gate in front of you."
        puts "You read a writing carved in it...#{@gates.name}...it says."
        puts "Chills go down your spine...and suddenly, you feel the need to urinate."
        puts "There is no keylock on #{@gates.name}, only three empty holes."
        stones = []
        stones << @player.find_player_item("Uon stone")
        stones << @player.find_player_item("Wot stone")
        stones << @player.find_player_item("Reid stone")
        if stones == [nil, nil, nil]
          puts "(Hmmm, what should I do with this?!)"
          puts "There must be a reason why these holes are carved in it.."
        elsif stones != ["Uon stone", "Wot stone", "Reid stone"]
          puts "It seems like I don't have enough stones."
        else
          puts "(You figure you have to put stones inside, so #{@gates.name} would respond.)"
          puts "(But does the order you put them inside matters?)"
          while @player.lives > 0 && @gates.state == "closed"
            puts "(Choose the order. Insert just the name of the stone: reid)"
            @gates.combo = []
            puts "Put the first stone: "
            while true
              print ">"
              first_stone = gets.chomp.downcase
              if first_stone == "uon" || first_stone == "wot" || first_stone == "reid"
                break
              else
                puts "(Enter just a name of the stone, like so - - > Wot)"
              end
            end
            puts "You first put #{first_stone}."
            @gates.combo << first_stone

            puts "Put a second stone: "
            while true
              print ">"
              second_stone = gets.chomp.downcase
              if second_stone == "uon" || second_stone == "wot" || second_stone == "reid"
                break
              else
                puts "(Enter just a name of the stone, like so - - > Wot)"
              end
            end
            puts "Secondly, you put #{second_stone}."
            @gates.combo << second_stone

            puts "Put the third stone: "
            while true
              print ">"
              third_stone = gets.chomp.downcase
              if third_stone == "uon" || third_stone == "wot" || third_stone == "reid"
                break
              else
                puts "(Enter just a name of the stone, like so - - > Wot)"
              end
            end
            puts "You put #{third_stone} at the end...and hope for the best."
            @gates.combo << third_stone

            if (@gates.combo != ["uon", "wot", "reid"]) && @player.lives == 3
              puts "Deep and menacing voice of old and rusty machinery yells out.."
              puts "Something moves inside a door."
              puts "O-oo, it doesn't sound good."
              @player.lives -= 1
            elsif (@gates.combo != ["uon", "wot", "reid"]) && @player.lives == 2
              puts "The shaft inside the #{@gates.name} moves further."
              puts "Your uncertainty grows bigger on you."
              puts "It must be some sort of massive locking mechanism!"
              puts "Your glorious adventure can not, WILL NOT, end HERE!"
              @player.lives -= 1
            elsif (@gates.combo != ["uon", "wot", "reid"]) && @player.lives == 1
              puts "...OMG...shit..."
              @player.lives -= 1
            else
              puts "GGGGGRRRRRR!!!!!! #{@gates.name} open!!!"
              @gates.state = "opened"
            end
          end
        end
      elsif choice == "3"
        break
      else
        puts "Smartass....."
      end
    end
  end

  def happy_end(player_name)
    puts "Congratulations #{player_name}! Like Simbad himself was your mentor!"
    puts "Go on you brave and witty soul! Let nothing stop you!!"
    in_game_time = (Time.now - @game_time).to_i / 60
    puts "> It took you [#{in_game_time} min] to complete the adventure.<"
    puts "You are serious gaming material!! GG M8!!" if in_game_time < 7
    exit
  end

  def sad_end(gate_name, player_name)
    puts "#{gate_name} clicks. Your tears trickle down your cheeks #{player_name}..there is always a second chance adventurer...always!"
    puts "Except this time. BUAHAHAHAAHAH!!!!!"
    in_game_time = (Time.now - @game_time).to_i / 60
    puts "> It took you [#{in_game_time} min] to NOT complete the adventure.<"
    exit
  end

  class Player
    attr_accessor :name, :items, :information,:lives, :location

    def initialize(name, items, information, lives)
      @name = name
      @items = items
      @information = information
      @lives = lives
    end

    def add_item_to_player_items(item)
      @items << item
    end

    def find_player_item(item_query)
      @items.detect { |item| item == item_query }
    end

    def add_player_information(information)
      @information << information
    end

    def find_player_information(information)
      @information.detect { |info| info == information}
    end
  end

  class Room
    attr_accessor :reference, :name, :description, :connections

    def initialize(reference, name, description, connections)
      @reference = reference
      @name = name
      @description = description
      @connections = connections
    end
  end

  class Items
    attr_accessor :name

    def initialize(name)
      @name = name
    end
  end

  class Door
    attr_accessor :name, :combo, :state

    def initialize(name, combo, state)
      @name = name
      @combo = combo
      @state = state
    end
  end

  class Creatures
    attr_accessor :name, :items, :information

    def initialize(name, items, information)
      @name = name
      @items = items
      @information = information
    end

    class Panda < Creatures
      def conversation
        puts "(You step bravely towards him.)"
        puts "(It looks like he is in some sort\nof meditation.)"

        while true
          puts "(What should you ask him?)"
          puts "|1.| Who are you?"
          puts "|2.| What do you know about that golden chest?"
          puts "|3.| Farewell, Great #{@name}!"
          print ">"
          ask = gets.chomp

          if ask == "1"
            puts "I am kung-fu panda. Also known as great warrior #{@name}."
            puts "I beat up a Rhino once. Not impressed? I beat up a giraffe to!"
            puts "You look like a nice lad......don't fuck with me."
            puts "What do you want?"
          elsif ask == "2"
            puts "I tried to break into it. I am sure something of great value is stored inside."
            puts "But you know I am a fighter, and a really strong one."
            puts "Not some disgusting thief. If you know what I mean that is.... "
            puts "*frowns*"
          elsif ask == "3"
            puts "Buzz-off kid.. I'm leveling-up here.."
            puts "Bring me trstika next time!"
            break
          else
            puts "Smartass..."
          end
        end
      end
    end

    class FoxyLady < Creatures

      def exit_method(current_room)
        current_room
      end

      def conversation(player)
        while true
          hAiRpIn = player.items.detect {|item| item == "Hairpin"}
          puts "You see a fine looking, well dressed fox lady."
          puts "Silk dress, shiny shoes, rings and things, hair stuffed with hairpins. It looks like a state of art."
          puts "She is finde indeed. You use your charm and find out that her name is Ice La Fox."
          puts "What a beautiful name, you think to yourself.\n\n"

          if !hAiRpIn
            puts "|1.| Dear maam Ice La Fox, i humbly admit I have not witnessed such a beauty to this day!"
            puts "Do you mind giving me a hairpin of yours so I shall reminisce this beauty for the rest of my life!?"
            puts "|2.| Do you know anything about this place dear maam?"
            puts "|3.| (You decide to pursue your adventure. Enough with this crazy talk.)"
          else
            puts "|1.| Do you know anything about this place dear maam?"
            puts "|2.| (You decide to pursue your adventure. Enough with this crazy talk.)"
          end
          print ">"
          choice = gets.chomp

          if choice == "1" && !hAiRpIn
            puts "Aren't you a sweet one?!"
            puts "There you go little boy. (She winks at you.)"
            puts "She gives you her hairpin."
            puts "Well done you stud! You give yourself a pat on the back."
            @items = @items - ["Hairpin"]
            player.add_item_to_player_items("Hairpin")
          elsif (choice == "2" && !hAiRpIn) || (choice == "1" && hAiRpIn)
            puts "This used to be an old mining site for a big factory whice is owned by a man who is my husband."
            puts "It is just a part of his international conglomerate. He is one of the wealthiest man in the world."
            puts "It is most natural that he owns the most beautiful women in the world, too. (she chuckels..)"
            puts "I am waiting for him. He just left to give a lecture to a site manager about some business stuff."
            puts "Did I mention he is very rich!?!"

            while true
              puts "|1.| Did you see anything around here that might be useful to a poor soul like me, maam la Fox?"
              puts "|2.| Would it happen that you know something about that locked chest in the Old room little further ahead, maam la Fox?"
              puts "(|3.| Go to the beginning of the conversation.)"
              print ">"

              question = gets.chomp

              if question == "1"
                puts "I noticed that box of old and dirty stuff there in the corner."
                puts "It seems to have contained things like an old tool, a handy bag, dusty books and such..."
                puts "I don't know who would want to dig inside it though."
                puts "It looks sooo...uggghhhh."
                if !player.information.detect { |info| info == "Bag"}
                  @information = @information - ["Bag"]
                  player.add_player_information("Bag")
                end
              elsif question == "2"
                puts "What chest!?"
                puts "I am no poor sould in need of some dirty and ragged stuff."
                puts "Phew..(She looks the other way..)"
              elsif question == "3"
                puts "(You decide to go to the beginning of the conversation."
                break
              else
                puts "Smartass....."
              end
            end
          elsif (choice == "3" && !hAiRpIn) || (choice == "2" && hAiRpIn)
            puts "(You can not listen to the word she says anymore...but she could be a fiiiine company.):O ^^"
            break
          else
            puts "Smartass....."
          end
        end
      end
    end
  end
end

def beginning
  puts ". . . . . I N  M E D I A S  (K )R E S . . . . .\n\n"
  puts Time.now
  puts "made by: daario daic\n\n"
  print "Player name > "
  player = gets.chomp.to_s
  puts "#{player} there are 4 rooms in this adventure: "
  puts "Lobby room"
  puts "Storage room"
  puts "Machinery room"
  puts "Old room"
  puts "As you wander you type only their first names, like so >Old<"
  puts "And instead of a room name you can type >items< to view, well your items."
  puts "Go on! May the Force guard your ass!!"
  puts "**  " * 20
  player
end

dun1 = Dungeon.new(beginning, [], [], 3, "Trichance", [], "closed")

dun1.add_a_room(:lobby_room,
                "Lobby room".upcase,
                "It looks like a room ingraved inside a cave, with a closet and usual furniture around.",
                { :storage => :storage_room, :machinery => :machinery_room, :old => :old_room })
dun1.add_a_room(:storage_room,
                "Storage Room".upcase,
                "There are a lot of boxes around...some are oragnized in stacks, and few of them scattered.",
                { :lobby => :lobby_room, :old => :old_room, :machinery => :machinery_room })
dun1.add_a_room(:machinery_room,
                "Machinery Room".upcase,
                "Room is full of old and rusted machinery..it must have been vital in keeping the mine operating..",
                { :lobby => :lobby_room, :old => :old_room, :storage => :storage_room })
dun1.add_a_room(:old_room,
                "Old Room".upcase,
                "Someone crafted a nice room inside a cave, but it is old and air is stale. There are huge and impressive gates in front of you. There are three oval holes in it and no lock..Hhmm...",
                { :storage => :storage_room, :machinery => :machinery_room, :lobby => :lobby_room })
dun1.add_creatures
dun1.add_items

dun1.start_room(:old_room)
